<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hiring_group_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sxz)trs/%&sE&(j{]H,a1Q4WaYHu`kn&uJ9NwV6{yWR|*g_OTr:krU4tA2([CCVl');
define('SECURE_AUTH_KEY',  'Rd78VI*0l-kf:Qu=:*@~](w<t9q#qXdEGvyT-^tk_NTsurB!~kP&4w3i&T7yPsb?');
define('LOGGED_IN_KEY',    '=`*`EIWRzo3xzJ=^[_*n*k0#3%ZX4thIDc.PgZ[Md[9P4t!|`:6d|J){?7GfKao5');
define('NONCE_KEY',        'S!Ym*,l}+D3_uEC]+2=wgY.=[Gk?}!U##Y0(XBa:1+ :F eOfsJ(QS>bj>VAv~,7');
define('AUTH_SALT',        '^D+P^ZeIRVL;8%7uS@<Mb`M[R*Klbh$Juj-]iv}/>OS{OF}.8YE9SOe8H=JHI+[_');
define('SECURE_AUTH_SALT', 'K3HVd 1Uik#,@CtVyRdrtk1_zxP)(UR_19f,HDe*y$nVPjCR*U7MuTzNv&|ua7?_');
define('LOGGED_IN_SALT',   's^/k44dWV/elUV:|wF3BuY]*oNeh uHNk@?QlpvU|i7eTQ#1geQ|+wq(Zs2<pDgC');
define('NONCE_SALT',       'E}B$M;v*^8{5+6UaZpIB2]kwCQs2{2F}1uD/P;A` :KCosuu4-^N@xIxq.ko e,R');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

<?php
/**
 * The front page template file
 * Template Name: Mission-Vission
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<!-- banner section starts here -->
<section>	
		<div class="mission-banner">
			<div class="banner-text text-center">
				<h1><?php the_field('banner_title') ?></h1>
    			<h3><?php the_field('banner_sub_title') ?></h3>
			</div>
		</div>		
</section>

<section>
	<div class="middle-content">
		<!-- why Thg inner sec starts here -->
		<div class="container core-container">
			<h2><?php the_field('page_heading') ?></h2>

			<div class="core-value">

			
			
				<div class="box-1">
					<?php

				// check if the repeater field has rows of data
				if( have_rows('mission_sec_1') ):

				 	// loop through the rows of data
				    while ( have_rows('mission_sec_1') ) : the_row();
				?>
					<div class="box-inner text-center <?php the_sub_field('light_inner') ?>">
						<img src="<?php the_sub_field('mission_image') ?>" alt="">
						<h3><?php the_sub_field('mission_heading') ?></h3>
						<p><?php the_sub_field('mission_content') ?></p>
					</div>

					<?php

					endwhile;

					else :

					    // no rows found

					endif;

				?>
				</div>
				
				
				<div class="box-2">

				

				<?php

				// check if the repeater field has rows of data
				if( have_rows('mission_sec_2') ):

				 	// loop through the rows of data
				    while ( have_rows('mission_sec_2') ) : the_row();
				?>

					<div class="box-inner text-center <?php the_sub_field('light_inner') ?>">
						<img src="<?php the_sub_field('mission_image') ?>" alt="">
						<h3><?php the_sub_field('mission_heading') ?></h3>
						<p><?php the_sub_field('mission_content') ?></p>
					</div>


				<?php

					endwhile;

					else :

					    // no rows found

					endif;

				?>
				</div>

				<div class="box-3">

				

				<?php

				// check if the repeater field has rows of data
				if( have_rows('mission_sec_3') ):

				 	// loop through the rows of data
				    while ( have_rows('mission_sec_3') ) : the_row();
				?>

					<div class="box-inner text-center <?php the_sub_field('light_inner') ?>">
						<img src="<?php the_sub_field('mission_image') ?>" alt="">
						<h3><?php the_sub_field('mission_heading') ?></h3>
						<p><?php the_sub_field('mission_content') ?></p>
					</div>


				<?php

					endwhile;

					else :

					    // no rows found

					endif;

				?>
				</div>
				
			</div>

				
		</div>
	</div>
</section>


	
<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead' , 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();

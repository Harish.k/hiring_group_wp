<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.2
 */

?>



<?php wp_footer(); ?>

<section>
	<footer>
		<!-- <div class="container no-padding">
			<div class="row m-0 pt-80 pb-80">
				<div class="col-md-4 col-sm-6 no-padding">
					<div class="footer-company-sec">
						<figure>
							<img src="<?php bloginfo('template_url') ?>/assets/images/logo.png" alt="">
						</figure>
						<p>Copyright @2017, The Hiring Group.<br>All Rights Reserved.</p>
					</div>
				</div>
				<div class="col-md-2 col-sm-6 no-padding">
					<div class="footer-quick-links">
						<h3>Quick Links</h3>
						<ul>
							<li><a href="#">Approach</a></li>
							<li><a href="#">Services</a></li>
							<li><a href="#">Engage</a></li>
							<li><a href="#">Resources</a></li>
							<li><a href="#">About Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 no-padding">
					<div class="footer-follow-sec">
						<h3>Follow Us On</h3>
						<ul>
							<li><a href="#"><span class="fa fa-facebook pl"></span>- Facebook</a></li>
							<li><a href="#"><span class="fa fa-twitter"></span>- Twitter</a></li>
							<li><a href="#"><span class="fa fa-linkedin"></span>- Linkedin</a></li>
							<li><a href="#"><span class="fa fa-instagram"></span>- Instagram</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 no-padding">
					<div class="footer-join-sec">
						<h3>Join Our Community</h3>
						<form>
							<p>Get the latest news, job posts and  updates from The Hiring Group</p>
							<input type="text" name="" placeholder="Email ID">
							<button type="" name="" value="">SUBSCRIBE</button>
						</form>
					</div>
				</div>
			</div>
		</div> -->

		<div class="container no-padding">
			<div class="row m-0 pt-80 pb-80">
				<div class="col-md-4 col-sm-6 no-padding">
					<div class="footer-company-sec">
						<figure>
							<a href="<?php echo home_url(); ?>"><img src="<?php the_field('logo', 'option') ?>" alt="logo"></a>
						</figure>
						<p><?php the_field('copy_text', 'option') ?><br><?php the_field('reserved_text', 'option') ?></p>
					</div>
				</div>		

				<div class="col-md-2 col-sm-6 no-padding">
					<div class="footer-quick-links">
						<h3><?php the_field('quick_heading', 'option') ?></h3>
							<?php

								wp_nav_menu( array(
								    
								   
								    'theme_location' => 'footer'

								) );

							?>
					</div>
				</div>

				<div class="col-md-3 col-sm-6 no-padding">
					<div class="footer-follow-sec">
						<h3><?php the_field('follow_heading', 'option') ?></h3>		
				
						<?php	

							wp_nav_menu( array(
						    
						   
						    'theme_location' => 'social'

							) );

		 				?>
		 			</div>
		 		</div>

		 		<div class="col-md-3 col-sm-6 no-padding">
					<div class="footer-join-sec">
						<h3><?php the_field('join_heading', 'option') ?></h3>
						<form>
							<p><?php the_field('join_message', 'option') ?></p>
							<!-- <input type="text" name="" placeholder="Email ID">
							<button type="" name="" value="">SUBSCRIBE</button> -->

							<?php echo do_shortcode('[email-subscribers namefield="NO" desc="" group="Public"]'); ?>
						</form>
					</div>
				</div>		

			</div>
		</div>
	</footer>
</section>

<script src="<?php bloginfo('template_url') ?>/assets/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/jquery.bxslider.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/jquery.magnific-popup.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/job_widget.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/custom.js"></script>
<script>
	jQuery(document).ready(function(){

      jQuery('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
		autoplay:true,
      	autoplayTimeout:20000,
	    dots:true,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        680:{
	            items:2
	        },
	        1000:{
	            items:3
	        }
	    }
	});

    jQuery(document).ready(function() {
	jQuery('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 479,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: true
	});
});
});
</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-53131001-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		  gtag('config', 'UA-53131001-1');
		</script> 
</body>
</html>

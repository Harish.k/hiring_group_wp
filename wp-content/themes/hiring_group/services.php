<?php
/**
 * The front page template file
 * Template Name: Our-Services
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!-- banner section starts here -->
<section>	
		<div class="banner-sec com_ban services_bn">
			<h1><?php the_field('banner_title') ?></h1>
			<h3><?php the_field('banner_sub_title') ?></h3>
		</div>		
</section>

<section>
	<div class="middle-content">
		<div class="container">
			<h2><?php the_field('page_heading') ?></h2>
			<div class="pt-30 text-center">
				<?php the_field('heading_content') ?>
			</div>
		</div>
<div class="find_job_list">
		<div class="container">
			<div class="div_block row">
				<div class="div_left col-sm-2">
					<img src="<?php the_field('service_list_image') ?>" alt="find_job">
				</div>
				<div class="div_right col-sm-10">
					<ul>
						<?php

							// check if the repeater field has rows of data
							if( have_rows('services_list') ):

							 	// loop through the rows of data
							    while ( have_rows('services_list') ) : the_row();
						?>
							<li>
								<img src="<?php the_sub_field('services_list_icon') ?>" alt="job_list">
								<h5><?php the_sub_field('services_list_heading') ?></h5>
								<?php the_sub_field('services_list_content') ?>
							</li>

						<?php

							endwhile;

							else :

							    // no rows found

							endif;

						?>	
						
					</ul>
				</div>
			</div>
		</div>
	</div> 
</div>
</section>

<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead' , 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();
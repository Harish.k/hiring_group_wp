<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

		<?php wp_head(); ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/fonts.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/magnific-popup.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/owl.carousel.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/assets/css/jquery.bxslider.min.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/owl.theme.default.min.css"/>

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,300,400,600,700" rel="stylesheet">
		<link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/assets/images//cropped-LogoEmblem-32x32.png" type="image/x-icon">
		<link rel="icon" href="<?php bloginfo('template_url') ?>/assets/images//favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/job_widget.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/custom.css">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/responsive.css">

	</head>

	<body <?php body_class(); ?>>

		<section class="head-sec">
			<header id="header" class="">
		<!-- nav class="navbar navbar-inverse">
			<div class="container-fluid sm-padding">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>                        
			      </button>
					<a class="navbar-brand" href="#"><img src="<?php bloginfo('template_url') ?>/assets/images/logo.png"></a>
				</div>
				  <div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="#">Home</a></li>
						<li><a href="#-1">Approach</a>
							<div class="drop-menu-outer">
								<div class="drop-menu">
									<a class="list-1" href="#">Why The Hiring Group?</a>
									<a class="list-2" href="#">Contractor Benefits</a>
								</div>
							</div>	
						</li>
						<li><a href="#">Services</a>
							<div class="drop-menu-outer">
								<div class="drop-menu">
									<a class="list-3" href="#">Employment Services</a>
									<a class="list-4" href="#">Focus Areas</a>
								</div>
							</div></li>
						<li><a href="#">Engage</a>
							<div class="drop-menu-outer">
								<div class="drop-menu">
									<a class="list-5" href="#">Find Great Jobs</a>
									<a class="list-6" href="#">Hire Great Contractors</a>
								</div>
							</div></li>
						<li><a href="#">Resources</a>
							<div class="drop-menu-outer">
								<div class="drop-menu">
									<a class="list-7" href="#">News & Events</a>
									<a class="list-8" href="#">Blog</a>
									<a class="list-9" href="#">Downloads</a>
								</div>
							</div>
						</li>
						<li><a href="#">About Us</a>
							<div class="drop-menu-outer">
								<div class="drop-menu">
									<a class="list-10" href="#">Mission & Values</a>
									<a class="list-11" href="#">Leadership</a>
									<a class="list-12" href="#">Contact Us</a>
								</div>
							</div>
						</li>
						<li class="menu-line"><img src="assets/images/menu-line.png"></li>
						<li class="sign-icon"><a href="#"><span class="fa fa-search"></span>Search Jobs</a></li>
						<li class="menu-line"><img src="assets/images/menu-line.png"></li>
						<li class="sign-icon"><a href="#"><span class="fa fa-envelope-o"></span>Email Us</a></li>
						<li class="menu-line last-line"><img src="assets/images/menu-line.png"></li>
						<li class="web-center-link"><a href="#" class=""><span>WEB CENTER</span></a></li>
					</ul>
				</div>
			</div>
		</nav> -->

		<nav class="navbar navbar-inverse" role="navigation">
			<div class="container-fluid sm-padding">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo home_url(); ?>">
						<img src="<?php the_field('logo', 'option') ?>" alt="logo">
					</a>
				</div>
				<?php
				wp_nav_menu( array(
					'theme_location'    => 'primary',
					'depth'             => 2,
					'container'         => 'div',
					'container_class'   => 'collapse navbar-collapse',
					'container_id'      => 'bs-example-navbar-collapse-1',
					'menu_class'        => 'nav navbar-nav navbar-right',
					'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
					'walker'            => new WP_Bootstrap_Navwalker())
				);
				?>
			</div>
		</nav>
	</header><!-- /header -->		
</section>
<?php
/**
 * The front page template file
 * Template Name: News & Blog
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<section>
	<!-- banner section starts here -->
		<div class="news-event-banner">
			<h1><?php the_field('banner_title') ?></h1>
    		<h3><?php the_field('banner_subtitle') ?></h3>
		</div>		
</section>

<section>
	<div class="middle-content news-events-container">
		<div class="container">
			<h2><?php the_field('filter_heading') ?></h2>
			<h5><?php the_field('filter_subhead') ?></h5>
	        <div class="row">
	        <div align="center" class="filter-heads">
				<!-- 	        
				<button class="btn btn-default filter-button" data-filter="blog">Blogs</button>
 		        
	            <button class="btn btn-default filter-button" data-filter="news-event">News</button>
   	            <button class="btn btn-default filter-button active" data-filter="all">All</button>
   	            -->
   	           

				<?php 
				$args = array('category');
				$tax = get_terms($args);
				foreach($tax as $t){
					$args = array(
						      'post_type' => 'news',
						      'tax_query' => array(
								array(
									'taxonomy' => 'category',
									'field' => 'slug',
									'terms' => array($t->slug)
								)
							),
						      'posts_per_page' => 1,
						      'post_status' => 'publish'
						      );
				$query = new WP_Query( $args );
				while ( $query->have_posts() ) {
					$query->the_post();
				?>

				<button class="btn btn-default filter-button" data-filter="<?php echo $t->name; ?>"> 
					<?php echo $t->name; ?> 
				</button>

				<?php
				}
						wp_reset_query();
				}
				?>
				
				<button class="btn btn-default filter-button active" data-filter="All">All</button>
	        </div>
	        <br/>


	        <?php 
			    query_posts( array( 'post_type' => 'news', 'showposts' => -1 , 'orderby' => 'date' , 'order' => 'DESC' ) );
			    if ( have_posts() ) : while ( have_posts() ) : the_post();
			        $categories = get_the_category();
			            foreach ( $categories as $category ) { 
			        $category->name; 
			    }             
			?>

			<?php if ($category->name == "ARTICLE") {  ?>
			<div class="filter_products filter ARTICLE">
            	<div class="filter_product_lg">
	            	<div class="img-box large_product_1" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
<!-- 		                <img src="assets/images/news-thumbnail_1.png" alt="" class="img-responsive">
-->		                <div class="img-overlay">
	              			<div class="img-description">
	              				

	              				<?php if( get_field('outside_url_link') ) { ?>
	
								<a href="<?php the_field('outside_url'); ?>" target="_blank"><?php the_title(); ?></a>
								
								<?php } else { ?>
				            		<a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a>
				            	<?php } ?>

	              				<div class="publish-date">
	              					<span><?php the_date(); ?></span>
	              				</div>
	              			</div>
	              		</div>
	              		<div class="img-box-tag">
	              			<img src="<?php the_field('tag_image') ?>" alt="">
	              		</div>	
	                </div>
	            </div>    
            </div>
            <?php } elseif($category->name == "NEWS") { ?>
            	<div class="filter_products filter NEWS" style="">
	            	<div class="filter_product_sm">
			            <div class="img-box">
			                <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive">
		              		<div class="img-overlay">
		              			<div class="img-description">
		              				<div class="publish-date">
		              					<span> <?php the_date(); ?> </span>
		              				</div>
		              			</div>
		              		</div>
		              		<div class="img-box-tag">
		              			<img src="<?php the_field('tag_image') ?>" alt="">
		              		</div>
			            </div>
			            <div class="news-content">
			            	<h3> <?php the_title(); ?> </h3>
			            	<?php the_field('short_description'); ?>
			            	<?php if( get_field('outside_url_link') ) { ?>
	
								<a href="<?php the_field('outside_url'); ?>" target="_blank">Read More</a>
								
							<?php } else { ?>
			            		<a href="<?php the_permalink(); ?>" target="_blank">Read More</a>
			            	<?php } ?>
			            </div>
			        </div>    
	            </div>

            <?php } elseif($category->name == "BLOG") { ?>

            <div class="filter_products filter BLOG">
	            	<div class="filter_product_sm">
			            
			            <div class="news-content">
			            	<h3><?php the_title(); ?></h3>
			            	<?php the_field('short_description'); ?>
			            	<?php if( get_field('outside_url_link') ) { ?>
	
								<a href="<?php the_field('outside_url'); ?>" target="_blank">Read More</a>
								
							<?php } else { ?>
			            		<a href="<?php the_permalink(); ?>" target="_blank">Read More</a>
			            	<?php } ?>
			            </div>
			        </div>    
	            </div>
	         <?php } ?>
			<?php endwhile; endif; wp_reset_query(); ?>
	           
	        </div>
	        <div class="load_more_sec">
				<button class="load_btn alert_btn"><?php the_field('view_more_button_text'); ?></button>
			</div>
   		</div>
	</div>
</section>

<?php get_footer();

<?php
/**
 * The front page template file
 * Template Name: Why-thg
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<section>
	<!-- banner section starts here -->
		<div class="why-banner">
			<div class="banner-text text-center">
				<h1><?php the_field('banner_title') ?></h1>
    			<h3><?php the_field('banner_sub_title') ?></h3>
			</div>
		</div>		
</section>

<section>
	<div class="middle-content">
		<!-- why Thg inner sec starts here -->
		<div class="container">
			<div class="row why-equation text-center pb-80 m-0">
				<h2><?php the_field('page_heading') ?></h2>
				<p><?php the_field('hiring_heading_content') ?></p>
				<p><?php the_field('hiring_heading_content_2') ?></p>

				<div class="col-md-4">
					<div class="eq-circle">
						<img src="<?php the_field('hiring_image_1') ?>" alt="">
						<h3><?php the_field('hiring_image_1_text') ?></h3>
					</div>
				</div>

				<div class="col-md-4">
					<div class="eq-circle eq-second">
						<img src="<?php the_field('hiring_image_2') ?>" alt="">
						<h3><?php the_field('hiring_image_2_text') ?></h3>
					</div>
				</div>

				<div class="col-md-4">
					<div class="eq-circle">
						<img src="<?php the_field('hiring_image_3') ?>" alt="">
						<h3><?php the_field('hiring_image_3_text') ?></h3>
					</div>
				</div>
				<a href="<?php the_field('explore_button') ?>"><?php the_field('explore_button_text') ?></a>
			</div>
		</div>
	</div>
</section>

<section class="war-sec">
	<div class="container">
		<div class="row pb-80 pt-80 m-0">
			<h2><?php the_field('talent_heading') ?></h2>
			<div class="col-md-7">
				<p class="first-p"><?php the_field('talent_content_1') ?></p>
				<p><?php the_field('talent_content_2') ?></p>
				<a href="<?php the_field('more_service_button') ?>"><?php the_field('more_service_button_text') ?></a>
			</div>
			<div class="col-md-5">
				<div class="war-right">
					<div class="war-list">
						<span><img src="<?php the_field('talent_flow_image_1') ?>" alt=""></span>
						<div class="list-point">
							<p><?php the_field('talent_flow_1_text') ?></p>
						</div>
					</div>
					<div class="war-list">
						<span><img src="<?php the_field('talent_flow_image_2') ?>" alt=""></span>
						<div class="list-point">
							<p><?php the_field('talent_flow_2_text') ?></p>
						</div>
					</div>
					<div class="war-list">
						<span><img src="<?php the_field('talent_flow_image_3') ?>" alt=""></span>
						<div class="list-point">
							<p><?php the_field('talent_flow_3_text') ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>
	
<section>
	<div class="container">
		<div class="row pt-80 pb-45">
			<div class="col-md-6 bussiness-partner">
				<h2><?php the_field('business_heading') ?></h2>
					<p><?php the_field('business_content') ?></p>
					<a href="<?php the_field('focus_link') ?>" class=""><?php the_field('focus_link_button') ?></a>
			</div>
			<div class="col-md-6">
				<div class="right">
					<figure>
						<img src="<?php the_field('business_partner_image') ?>" alt="">						
					</figure>
				</div>
			</div>
		</div>
	</div>
	
</section>

<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead' , 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();


<?php
/**
 * The front page template file
 * Template Name: Hire-Contractors
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!-- banner section starts here -->
<section>	
		<div class="banner-sec com_ban hire_contractors">
			<h1><?php the_field('banner_title') ?></h1>
			<h3><?php the_field('banner_sub_title') ?></h3>
		</div>		
</section>

<section>
	<div class="middle-content hire_cont">
		<div class="container">
			<h2><?php the_field('page_heading') ?></h2>
			<div class="pt-30 text-center">
				<?php the_field('heading_content') ?>
			</div>
			<div class="pt-80 text-center">
				<a class="popup-youtube" href="<?php the_field('it_staffing_video') ?>">
            	   <img src="<?php the_field('it_staffing_image') ?>" alt="vdo_img">
            	</a>
			</div>
		</div>
      
	</div>
</section>

<section class="bg_f9f9f9 pt-80 pb-45 middle-content cssanimations">
	<div class="container">
		<h2><?php the_field('work_client_heading') ?></h2>

		<section id="cd-timeline" class="cd-container">
			<?php

			// check if the repeater field has rows of data
			if( have_rows('work_flow') ):

			 	// loop through the rows of data
			    while ( have_rows('work_flow') ) : the_row();
			?>
			<div class="cd-timeline-block">

	          <div class="slides_div <?php the_sub_field('work_flow_class') ?>">

				<div class="cd-timeline-img <?php the_sub_field('work_animation_class') ?>">
					<img src="<?php the_sub_field('work_flow_icon') ?>" alt="Picture">
				</div> <!-- cd-timeline-img -->

				<div class="cd-timeline-content <?php the_sub_field('work_animation_class') ?>">
					<h4><?php the_sub_field('work_flow_heading') ?></h4>
					<p><?php the_sub_field('work_flow_content') ?></p>
					
				</div> <!-- cd-timeline-content -->

				</div>
			</div> <!-- cd-timeline-block -->

			<?php

				endwhile;

				else :

				    // no rows found

				endif;

				?>	

	</section> <!-- cd-timeline -->
	</div>
</section>

<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead' , 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();
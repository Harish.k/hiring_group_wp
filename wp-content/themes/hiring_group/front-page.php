<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- banner section starts here -->
		<section>
			<div class="banner-sec">
				<h1> <?php the_field('banner_title') ?> </h1>
	    		<h3> <?php the_field('banner_sub_title') ?> </h3>
				<div class="banner-btn text-center">
					<a href="<?php the_field('banner_button_1_link') ?>"> <?php the_field('banner_button_1_text') ?> </a>
					<a class="job-btn" href="<?php the_field('banner_button_2_link') ?>">  <?php the_field('banner_button_2_text') ?> </a>
				</div>
			</div>
		</section>

<section>
	<div class="middle-content">
		<div class="container">
			<h2><?php the_field('page_heading') ?></h2>
			<div class="row pt-80">
				<div class="col-md-6">
					<div class="left">
						<?php the_field('hire_content') ?>
						<a href="<?php the_field('hire_button_link') ?>" class=""><?php the_field('hire_button_text') ?></a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="right">
						<a class="vdo_img popup-youtube video_icon" href="<?php the_field('hire_video') ?>">
            	   			<img src="<?php the_field('hire_video_image') ?>" alt="vedios">
            	   		</a>
					</div>
				</div>
			</div>	
		</div>

		<!-- Employee solutions starts here -->

		<div class="container-fluid no-padding">
			<div class="row pt-80 m-0">
				<div class="col-lg-6 col-md-12 no-padding employee-sol">
					<div class="right">
						<!-- <figure>
							<img class="img-responsive" src="assets/images/employee-sol-left-img.png" alt="">							
						</figure> -->
						<div class="sol-overlay">
							<h4><?php the_field('employment_heading_1') ?></h4>
							<h5><?php the_field('employment_heading_2') ?></h5>
							<a href="<?php the_field('employment_button_link') ?>"><?php the_field('employment_button') ?></a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 no-padding">
					<div class="row m-0">

						<?php

							// check if the repeater field has rows of data
							if( have_rows('employment_solution') ):

							 	// loop through the rows of data
							    while ( have_rows('employment_solution') ) : the_row();
							?>
							<div class="col-md-6 col-sm-6 no-padding">
								<div class="sol-box box-2 box-3">
									<figure>
										<img src="<?php the_sub_field('employment_image') ?>" alt="">
									</figure>
									<h3><?php the_sub_field('employment_title') ?> </h3>
									<p><?php the_sub_field('employment_description') ?></p>  
								</div>
							</div>

							<?php
							       

							    endwhile;

							else :

							    // no rows found

							endif;

							?>
						
					</div>
				</div>
			</div>
		</div>

		<!-- stay-know starts here -->

		<div class="container">
			<div class="row pt-80 pb-80">
				<div class="col-md-6 stay-know">
					<h2><?php the_field('stay_heading') ?></h2>
					<p><?php the_field('stay_subhead') ?></p>
					<div class="left">
						<p><?php the_field('stay_content') ?></p>
						<a href="<?php the_field('stay_link') ?>" class=""><?php the_field('stay_link_text') ?></a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="right">
						<figure>
						<img src="<?php bloginfo('template_url') ?>/assets/images/stay-know-img.png"></figure>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container-fuild no-padding download-info">
		<div class="row m-0 pt-80 pb-80">
			<div class="col-lg-7 col-md-12">
				<div class="left">
					<div class="row m-0">
						<div class="col-md-4 col-sm-4 text-center">
							<div class="img-box">
								<img src="<?php the_field('founder_image'); ?>" class="" alt="">
								<h5><?php the_field('founder_name') ?></h5>
								<h6><?php the_field('founder_company') ?><br><?php the_field('founder_designation') ?></h6>
							</div>
						</div>
						<div class="col-md-8 col-sm-8 no-padding">
							<h4><?php the_field('industry_heading') ?></h4>
							<p><?php the_field('industry_subhead') ?></p>
							<p class="sm-text"><?php the_field('industry_content') ?></p>
							<a href="<?php the_field('industry_download') ?>" class="" download> DOWNLOAD NOW</a>
						</div>	
					</div>	
				</div>
			</div>
			<div class="col-lg-5 col-md-12 text-center">
				<div class="right">
					<a class="vdo_img popup-youtube video_icon" href="<?php the_field('industry_video') ?>">
            	   		<img src="<?php the_field('industry_video_image') ?>" alt="vedios">
            	   	</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="testimonials-sec pt-80 pb-80">
	<div class="container">
			<h3>TESTIMONIALS</h3>
			<div class="owl-carousel owl-theme">
				
				<?php $args = array( 'post_type' => 'testimonials', 'posts_per_page' => -1 );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
				?>

				<div class="test-box item">
					<div class="test-user-img-box">
					    <figure>
							<img src="<?php the_post_thumbnail_url(); ?>" alt="">
					    </figure>
					</div>
					<div class="client-head">
					   	<h5> <?php the_title(); ?> </h5>
					  	<p> <?php the_field('designation') ?> </p>
					</div>
					<div class="test-text-box">
						<?php the_content(); ?>
					</div>
				</div>

				<?php	  
					endwhile;
					wp_reset_query();
				?>

			</div>
	</div>
</section>

<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading') ?></h3>
			<p><?php the_field('form_subhead') ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();

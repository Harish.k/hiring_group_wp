<?php 
/*
Template Name: About Us
*/
get_header();
?>

<!-- Banner -->

<div class="banner-sec com_ban about">
	<h1> <?php the_title(); ?></h1>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php
    	endwhile; 
    	wp_reset_query(); 
    ?>
</div>

<!-- Banner ends here -->


<!-- About us content -->

<section>
	<div class="middle-content">
		<div class="container">
			<h2> <?php the_field('about_title') ?> </h2>
			<div class="pt-30 text-center">
				<?php the_field('about_description') ?>
			</div>

			<div class="row pt-80 pb-80">
				<?php
					if( have_rows('features') ):
				    while ( have_rows('features') ) : the_row();
    			?>
				<div class="col-sm-4">
					<div class="facilities_div text-center">
						<img src="<?php the_sub_field('feature_image') ?>" alt="features">
						<h5> <?php the_sub_field('feature_title') ?> </h5>
					</div>
				</div>
				<?php 
				    endwhile;
					else :
					endif;
				?>
			</div>
		</div>

        <div class="bg_f9f9f9 center_left ">
		<div class="container">
			<div class="row pt-80 pb-80">
				<div class="col-md-6 ">
					<h2> <?php the_field('better_way_title') ?> </h2>
					<div class="left">
						<?php the_field('better_way_description') ?>
						<a href="<?php the_field('better_way_button_link') ?>" class="btn"> <?php the_field('better_way_button_text') ?> </a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="right">
						<span>
							<img src="<?php the_field('better_way_image') ?>" alt="">						
						</span>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</section>

<!-- About us content ends -->



<!-- Our clients section -->
<section class="middle-content clients pb-80"> 
	<div class="container">
	<h2><?php the_field('our_clients_main_title') ?></h2>
		<div class="row">
			<?php
				if( have_rows('our_clients') ):
		    	while ( have_rows('our_clients') ) : the_row();
			?>
			<div class="col-sm-4">
				<div class="bg_box text-center">
				<img src="<?php the_sub_field('our_clients_image') ?>" alt="">
					<p><?php the_sub_field('our_client_title') ?></p>
				</div>
			</div>
			<?php 
			    endwhile;
				else :
				endif;
			?>
		</div>
	</div>
</section>
<!-- Our clients section -->



<!-- hire and find contractors -->

<section class="hire_find ">
	<div class="container-fuild no-padding download-info">
		<div class="flex_box">
			
				<div class="bg_img left" style="background-image:url(<?php the_field('hire_contractors_back_image') ?>);">
					<h2> <?php the_field('hire_contractors_title') ?> </h2>
					<?php the_field('hire_contractors_desc') ?>
					<a href="<?php the_field('hire_contractors_button_link') ?>"> <?php the_field('hire_contractors_button_text') ?> </a>
				</div>
		
			
			<div class="bg_img left right" style="background-image:url(<?php the_field('find_job_back_image') ?>);">
				<h2> <?php the_field('find_job_title') ?> </h2>
				<?php the_field('find_job_desc') ?>
				<a href="<?php the_field('find_job_button_link') ?>"> <?php the_field('find_job_button_text') ?> </a>
			</div>
			
		</div>
	</div>
</section>

<!-- hire and find contractors ends here -->


<!-- Leader ship section -->

<section class="leadership  middle-content pt-80 pb-80" style="margin-top: 0;">
	<div class="container">
		<h2> <?php the_field('leadership_main_title') ?> </h2>
		<div class="row">
			<?php
				if( have_rows('leaders') ):
		    	while ( have_rows('leaders') ) : the_row();
			?>
			<div class="col-sm-6">
				<div class="box_480">
				<div class="leaders">
					<span>
						<img src="<?php the_sub_field('leader_image') ?>">
					
					</span>
						<div>
						<h4> <?php the_sub_field('leader_name') ?> </h4>
						<p> <?php the_sub_field('leader_designation') ?> </p>
						<a href="mailto:<?php the_sub_field('leader_mail') ?>"><i class="fa fa-envelope" aria-hidden="true"></i></a>
						<a href="<?php the_sub_field('leader_linkedin_url') ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</div>
					</div>
				</div>
			</div>
			<?php 
			    endwhile;
				else :
				endif;
			?>
			<div class="col-sm-12 left text-center">
				<a href="<?php the_field('more_button_link') ?>" class="btn"> <?php the_field('more_button_text') ?> </a>
			</div>
		</div>
	</div>
</section>

<!-- Leadership section ends here -->


<!-- Contact us -->
<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead', 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Contact us -->


<?php get_footer(); ?>
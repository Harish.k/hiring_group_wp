<?php 
/*
Template Name: Focus Areas
*/
get_header();
?>


<style type="text/css">

.tab-content ul ul > li {
    float: left;
    width: 33%;
    border: none;
    position: relative;
    display: block;
    border-bottom: 1px solid #edecec;
    padding: 18px 12px 18px 22px;
    font-family: 'Proxima-Nova-Light', sans-serif;
    font-weight: 300;
    font-size: 18px;
    color: #666666;
}
.tab-content ul ul > li:last-child {
    border-bottom: 1px solid #eee !important;
}
</style>





<!-- focus banner -->
<div class="focus-banner">
	<div class="banner-text text-center">
		<h1> <?php the_title(); ?></h1>
		<h3> <?php the_field('sub_title') ?> </h3>
	</div>
</div>
<!-- focus banner -->


<!-- focus areas content -->
<section class="focus-sec">
	<div class="middle-content">
		<div class="container">
			<h2> <?php the_field('content_heading') ?> </h2>
			<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
			<?php
		    	endwhile; 
		    	wp_reset_query(); 
		    ?>
		</div>	
	</div>
</section>
<!-- focus areas content -->


<!-- tabs and pills -->
<section>
	<div class="tabs-section pb-60 text-center">
		<div class="tabs-head">
			<div class="container tabs-head-container">
				<ul class="nav nav-pills">
					<!-- <li class="no-line active"><a data-toggle="pill" href="#menu1"><img src="assets/images/tabs_icon_1.png" alt=""> Engineering Skill Sets</a></li>
					-->
					<?php
						if( have_rows('all_skills') ):
						$i=1;
				    	while ( have_rows('all_skills') ) : the_row();
				    	
	    			?>
					<li><a data-toggle="pill" href="#menu<?php echo $i; ?>"><img src="<?php the_sub_field('skills_icon') ?>" alt=""> <?php the_sub_field('skills_title') ?> </a></li>
					<?php 
						$i++;
					    endwhile;
						else :
						endif;
						
					?>
				</ul>
			</div>
		</div>
		<div class="tab-content">
			<?php
				if( have_rows('all_skills') ):
				$i=1;
		    	while ( have_rows('all_skills') ) : the_row();	
			?>

			<div id="menu<?php echo $i; ?>" class="tab-pane fade custom-tab-box">
				<ul class="list-unstyled">
					
					<li>
						<ul class="list-unstyled list-inline">
							<?php
								if( have_rows('skills_list') ):
						    	while ( have_rows('skills_list') ) : the_row();	
							?>
							<li> <?php the_sub_field('skill'); ?> </li>
							<?php
							    endwhile;
								else :
								endif;
							?>
						</ul>
					</li>
					
				</ul>
			</div>

			<?php
				$i++;
			    endwhile;
				else :
				endif;
			?>

		</div>
	</div>
</section>

<!-- Tabs and pills -->


<!-- Contact us -->
<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead', 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Contact us -->

<?php get_footer(); ?>

<script type="text/javascript">
	jQuery('.nav.nav-pills li:first-child').addClass('active no-line');
	jQuery('.tab-content .custom-tab-box:first-child').addClass('active in');
</script>
<?php 
/*
Template Name: Downloads
*/
get_header();
?>
<style type="text/css">
.library .col-sm-4:nth-child(n+4) {
    display: none;
}
</style>
<!-- Banner -->

<div class="banner-sec com_ban downloads">
	<h1> <?php the_title(); ?></h1>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php
    	endwhile; 
    	wp_reset_query(); 
    ?>
</div>

<!-- Banner ends here -->


<section>
	<div class="middle-content">

		<!-- Video library starts here-->

		<div class="container">
			<h2> <?php the_field('video_library_title') ?> </h2>
			
            <div class="video_library library pb-80">
            	<div class="row">
            		<?php 
						$query = new WP_Query( array('post_type' => 'videos', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' ) );
						while ( $query->have_posts() ) : $query->the_post();
					?>
            	   	<div class="col-sm-4">
            	   		<div class="vdo_box text-center">
            	   			<a class="vdo_img popup-youtube" href="<?php the_field('url'); ?>">
            	   				<img src="<?php the_post_thumbnail_url(); ?>" alt="vedios_thumbnail">
            	   			</a>
            	   			<div class="des_box text-left">
            	   				<div>
            	   					<div>
            	   						<p class="des_box_vdo"> <?php the_title(); ?></p>
            	   					</div>
            	   					
            	   				</div>
            	   			</div>
            	   		</div>
            	   	</div>

            	   	<?php wp_reset_postdata();
					endwhile; ?>

            	</div> 
            	<div class="left pt-45 text-center">
            		<a href="<?php the_field('view_more_link');?>" class="caps" target="_blank">View More</a>
            	</div>                             
            </div>
		</div>

		<!-- video library ends here -->


		<!-- Information library starts here -->
		<div class="bg_f9f9f9 pt-80 pb-80">
			<div class="container">
			<h2> <?php the_field('information_library_title') ?> </h2>
				<div class="information_library library">
            	<div class="row">

            		<?php 
						$query = new WP_Query( array('post_type' => 'information', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'ASC' ) );
						while ( $query->have_posts() ) : $query->the_post();
					?>

            	   	<div class="col-sm-4">
            	   		<div class="vdo_box text-center">
            	   			<span class="vdo_img">
            	   				<img src="<?php the_post_thumbnail_url(); ?>" alt="information_thumbnail">
            	   			</span>
            	   			<div class="des_box info_box_download text-left">
            	   				<div>
            	   					<div>
            	   						<p> <?php the_title(); ?> </p>
            	   					</div>
            	   					<div>
            	   						<a href="<?php the_field('add_file'); ?>" class="dwn" download></a>
            	   					</div>
            	   				</div>
            	   			</div>
            	   		</div>
            	   	</div>

            	   	<?php wp_reset_postdata();
				endwhile; ?>
            	   	
            	</div>    
            	<div class="left pt-45 text-center">
            		<a href="javascript:void(0)" class="caps alert_btn">View More</a>
            	</div>                            
            </div>
			</div>
		</div>

		<!-- Information library ends here -->
	</div>
</section>


<!-- Contact us -->
<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead', 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Contact us -->

<?php get_footer(); ?>

<script type="text/javascript">
	
	jQuery('.information_library .caps').click(function(){
		jQuery('.information_library .col-sm-4:nth-child(n+4)').toggle();
		//jQuery(this).toggle().html('View Less');
		jQuery(this).text(jQuery(this).text() == 'View More' ? 'View Less' : 'View More');
		return false;
	});
</script>
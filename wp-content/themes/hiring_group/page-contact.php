<?php
/*
	Template Name: contact
 */

get_header(); ?>
<!-- banner section starts here -->
		<section>
			<div class="banner-sec">
				<h1> <?php the_field('banner_title') ?> </h1>
	    		<h3> <?php the_field('banner_sub_title') ?> </h3>
				<div class="banner-btn text-center">
					<a href="<?php the_field('banner_button_1_link') ?>"> <?php the_field('banner_button_1_text') ?> </a>
					<a class="job-btn" href="<?php the_field('banner_button_2_link') ?>">  <?php the_field('banner_button_2_text') ?> </a>
				</div>
			</div>
		</section>

<section>
	<div class="middle-content">
		<div class="container">
			<h2>WE ARE <span>THE HIRING GROUP</span></h2>
			<div class="row pt-80">
				<div class="col-md-6">
					<div class="left">
						<p>The Hiring Group is a technical staffing and recruiting agency with locations throughout the U.S. We help companies hire and retain top IT and Engineering employees. Our mantra is <span>"Great Contractors Deserve Great Benefits.We offer the best benefits in the industry including medical, 401k (with company match) and paid time off (PTO)."</span></p>
						<a href="#" class="">LEARN MORE</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="right">
						<!-- <video width="556" height="306" poster="assets/images/video-img-1.jpg" controls="">
							<source src=".mp4" type="video/mp4">
							<source src=".ogg" type="video/ogg">
							Your browser does not support the video tag.
						</video> -->
						<!-- <div class="playpause"></div> -->
						<div class="video">
    						<img src="<?php bloginfo('template_url') ?>/assets/images/video-img-1.jpg">
    						<!-- <iframe width="940" height="529" src="http://www.youtube.com/embed/rRoy6I4gKWU?autoplay=1" frameborder="0" allowfullscreen></iframe>-->
						</div>
					</div>
				</div>
			</div>	
		</div>

		<!-- Employee solutions starts here -->

		<div class="container-fluid no-padding">
			<div class="row pt-80 m-0">
				<div class="col-lg-6 col-md-12 no-padding employee-sol">
					<div class="right">
						<!-- <figure>
							<img class="img-responsive" src="assets/images/employee-sol-left-img.png" alt="">							
						</figure> -->
						<div class="sol-overlay">
							<h4>EMPLOYMENT SOLUTIONS</h4>
							<h5>TO KEEP YOU MOVING FORWARD</h5>
							<a href="services.html">Lear More</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 no-padding">
					<div class="row m-0">

						<?php

							// check if the repeater field has rows of data
							if( have_rows('employment_solution') ):

							 	// loop through the rows of data
							    while ( have_rows('employment_solution') ) : the_row();
							?>
							<div class="col-md-6 col-sm-6 no-padding">
								<div class="sol-box box-3">
									<figure>
										<img src="<?php the_sub_field('employment_image') ?>" alt="">									
									</figure>
									<h3> </h3>
									<p> </p>  
								</div>
							</div>

							<?php
							       

							    endwhile;

							else :

							    // no rows found

							endif;

							?>
						
					</div>
				</div>
			</div>
		</div>

		<!-- stay-know starts here -->

		<div class="container">
			<div class="row pt-80 pb-80">
				<div class="col-md-6 stay-know">
					<h2><span>STAY</span> IN THE KNOW</h2>
					<p>Resources to keep you a step ahead.</p>
					<div class="left">
						<p>Get the latest hiring trends, interview tips and  industry info to stay current - visit The Hiring Group resource portal!</p>
						<a href="#" class="">VISIT RESOURCE PORTAL</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="right">
						<figure>
							<img src="<?php bloginfo('template_url') ?>/assets/images/stay-know-img.png" alt="">						
						</figure>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container-fuild no-padding download-info">
		<div class="row m-0 pt-80 pb-80">
			<div class="col-lg-7 col-md-12">
				<div class="left">
					<div class="row m-0">
						<div class="col-md-4 col-sm-4 text-center">
							<div class="img-box">
								<img src="<?php bloginfo('template_url') ?>/assets/images/download-thumb.png" class="" alt="">
								<h5>Chris Yarrow</h5>
								<h6>The Hiring Group<br>Co-Founder & Author</h6>
							</div>
						</div>
						<div class="col-md-8 col-sm-8 no-padding">
							<h4>Free Download! <span>New Industry Brief:</span> </h4>
							<p>4 Questions Every IT Professional Should Ask Before  Accepting A Contract Role.</p>
							<p class="sm-text">If you're an IT professional considering a contract role with a staffing company, are you asking the right questions? Download this brief by The Hiring Group Co-Founder and Managing Partner Chris Yarrow to learn what questions you need to ask to get the most out of this next career move.</p>
							<a href="#" class="">DOWNLOAD NOW</a>
						</div>	
					</div>	
				</div>
			</div>
			<div class="col-lg-5 col-md-12 text-center">
				<div class="right">
					<!-- <video width="573" height="362" poster="assets/images/video-img-2.gif" controls>
						<source src=".mp4" type="video/mp4">
						<source src=".ogg" type="video/ogg">
						Your browser does not support the video tag.
					</video> -->
					<!-- <div class="playpause"></div> -->
					<div class="video">
						<img src="<?php bloginfo('template_url') ?>/assets/images/video-img-2.jpg">
						<!-- <iframe width="940" height="529" src="http://www.youtube.com/embed/rRoy6I4gKWU?autoplay=1" frameborder="0" allowfullscreen></iframe>-->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="testimonials-sec pt-80 pb-80">
	<div class="container">
			<h3>TESTIMONIALS</h3>
			<div class="owl-carousel owl-theme">
				
				<?php $args = array( 'post_type' => 'testimonials', 'posts_per_page' => -1 );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
				?>

				<div class="test-box item">
					<div class="test-user-img-box">
					    <figure>
							<img src="<?php the_post_thumbnail_url(); ?>" alt="">
					    </figure>
					</div>
					<div class="client-head">
					   	<h5> <?php the_title(); ?> </h5>
					  	<p> <?php the_field('designation') ?> </p>
					</div>
					<div class="test-text-box">
						<?php the_content(); ?>
					</div>
				</div>

				<?php	  
					endwhile;
				?>

			</div>
	</div>
</section>

<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><span>ENGAGE</span> WITH US</h3>
			<p>Hire great contractors or find a great job.<span> We’re here to help. Let's start the conversation.</span></p>
			<div class="container">
				<div class="row m-0">
					<form>
						<input type="text" name="" value="" placeholder="Full Name">
						<input type="text" name="" value="" placeholder="Company">
						<input type="text" name="" value="" placeholder="Title">
						<input type="text" name="" value="" placeholder="Phone">
						<input type="text" name="" value="" placeholder="Email">
						<select name="">
							<option value="">I am looking for</option>
							<option value="">Find a Great Job</option>
							<option value="">Hire Great Contractors</option>
						</select>
						<button type="submit" value="" name="">SEND</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();

<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
 <section>
 <!-- banner section starts here -->
		<div class="news-event-banner">
			<h1><?php the_field('banner_title', 148) ?></h1>
    		<h3><?php the_field('banner_subtitle' , 148) ?></h3>
		</div>		
</section>
<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
			?>
<section class="event-detail-sec <?php the_field('smaller_height')?>">
	<div class="container">
		<div class="detail-container">
			<div class="detail-container-header">
				<div class="header-top">
					<div class="left">
						<div class="date-place-sec">
							<h5><?php the_field('date_place') ?></h5>
						</div>
					</div>
					<div class="right">
						<div class="author-sec">
							<div class="author-img-box">
								<figure>
									<img src="<?php the_field('author_image') ?>" alt="">
								</figure>	
							</div>
							<div class="author-detail-box">
								<h5><?php the_field('author_text') ?></h5>
								<h6><?php the_field('author_name') ?></h6>
								<p><?php the_field('author_position') ?></p>
							</div>
						</div>
					</div>
				</div>
				<h2><?php the_title(); ?></h2>
				<?php the_field('long_description') ?>
				<div class="events-social-container">
					<div class="left">
						<ul class="list-inline list-unstyled">
						<?php
							// check if the repeater field has rows of data
							if( have_rows('share_tags') ):

							 	// loop through the rows of data
							    while ( have_rows('share_tags') ) : the_row();
							?>
							<li class="<?php the_sub_field('tag_only')?>"><span><img src="<?php the_sub_field('tag_image') ?>" alt=""></span><span><?php the_sub_field('tag_number') ?></span></li>
							<?php

							endwhile;
							else :
						    // no rows found
							endif;
							?>

						</ul>
					</div>
					<div class="right">
						<ul>
							<?php
							// check if the repeater field has rows of data
							if( have_rows('share_tags_right') ):

							 	// loop through the rows of data
							    while ( have_rows('share_tags_right') ) : the_row();
							?>

							<li class="<?php the_sub_field('tag_only_right') ?>"><span class="<?php the_sub_field('auto_width') ?>"><img src="<?php the_sub_field('tag_right_image') ?>" alt=""></span><span class="<?php the_sub_field('auto_width') ?>"><?php the_sub_field('tag_right_text') ?></span></li>

							<?php

							endwhile;
							else :
						    // no rows found
							endif;
							?>
						</ul>
					</div>	
				</div>
			</div>  <!-- detail-container-header -->
			<div class="<?php the_field('detail_container')?>">
				<div class="<?php the_field('event_mid_banner')?>"></div>
				<div class="<?php the_field('highlight_sec')?>">
					<h3><?php the_field('highlights_heading')?></h3>
					<span><img src="<?php the_field('highlights_commas')?>" alt=""></span>
					<ul>
						<?php

							// check if the repeater field has rows of data
							if( have_rows('highlight_list') ):

							 	// loop through the rows of data
							    while ( have_rows('highlight_list') ) : the_row();
							?>
							<li><?php the_sub_field('list_item') ?></li>
							<?php

							endwhile;
							else :
						    // no rows found
							endif;
						?>
					</ul>
				</div>
			</div>  <!-- detail-container-mid -->

			<div class="<?php the_field('article_content_sec')?>">
				<?php

			// check if the repeater field has rows of data
			if( have_rows('article_list') ):

			 	// loop through the rows of data
			    while ( have_rows('article_list') ) : the_row();
			?>
				<?php the_sub_field('list_item_heading') ?>
				<p class="<?php the_sub_field('list_padding') ?>"><?php the_sub_field('list_item_content') ?></p>
				<?php

				endwhile;
				else :

				    // no rows found

				endif;
				?>
			</div>
			<div class="<?php the_field('blog_content_sec')?>">
				<ol>
				<?php

			// check if the repeater field has rows of data
			if( have_rows('main_content') ):

			 	// loop through the rows of data
			    while ( have_rows('main_content') ) : the_row();
			?>
				<li>
				<?php the_sub_field('list_heading') ?>
				<?php the_sub_field('list_main_text') ?>
				</li>
				<?php

				endwhile;

				else :

				    // no rows found

				endif;

				?>
				</ol>

			</div>


			<div class="events-tags">
				<ul class="list-unstyled list-inline">
				<h4 class="<?php the_field('tag_head')?>"><?php the_field('tag_head_text')?></h4>
				<?php

					// check if the repeater field has rows of data
					if( have_rows('news_tags') ):

					 	// loop through the rows of data
					    while ( have_rows('news_tags') ) : the_row();
					?>
					<li><a class="<?php the_sub_field('tag_status') ?>" href="<?php the_sub_field('tag_link') ?>"><?php the_sub_field('tag_text') ?></a></li>
					<?php

					endwhile;
					else :
					    // no rows found

					endif;
					?>
					
				</ul>
			</div>

			<div class="outer-social-link-container">
			<ul>
				<?php

					// check if the repeater field has rows of data
					if( have_rows('tags_social') ):

					 	// loop through the rows of data
					    while ( have_rows('tags_social') ) : the_row();
				?>
				<li><a href="<?php the_sub_field('tag_social_link') ?>" target="_blank"><span class="<?php the_sub_field('tag_social_icon') ?>"></span></a></li>
				<?php

					endwhile;
					else :
					    // no rows found

					endif;
					?>
	
			</ul>
		</div>

		</div> <!-- detail-container -->
		
	</div> 
</section>


	
		<?php	
				
			endwhile; // End of the loop.
		?>



<?php get_footer();

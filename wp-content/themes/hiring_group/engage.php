<?php
/*
 * Template Name: Search Jobs
*/

get_header(); ?>

<!-- banner section starts here -->
<section>
	<!-- banner section starts here -->
	<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
	<div class="why-banner search-jobs" style="background-image: url('<?php echo $backgroundImg[0]; ?>');">
		<div class="banner-text text-center">
			<h1><?php the_field('banner_title') ?></h1>
			<h3><?php the_field('banner_sub_title') ?></h3>
		</div>
	</div>		
</section>

<section>
	<div class="middle-content">
		<div class="container">
			<h2><?php the_field('page_heading') ?></h2>
			<div class="pt-30 text-center pb-80">
				<?php the_field('heading_content') ?>
			</div>
		</div>
      	<!-- why the hiring group -->
    </div>
</section>
<section class="testimonials-sec pt-80 pb-80">
	<div class="container">
			<h3>TESTIMONIALS</h3>
			<div class="owl-carousel owl-theme">
				
				<?php $args = array( 'post_type' => 'testimonials', 'posts_per_page' => -1 );
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
				?>

				<div class="test-box item">
					<div class="test-user-img-box">
					    <figure>
							<img src="<?php the_post_thumbnail_url(); ?>" alt="">
					    </figure>
					</div>
					<div class="client-head">
					   	<h5> <?php the_title(); ?> </h5>
					  	<p> <?php the_field('designation') ?> </p>
					</div>
					<div class="test-text-box">
						<?php the_content(); ?>
					</div>
				</div>

				<?php	  
					endwhile;
					wp_reset_query();
				?>

			</div>
	</div>
</section>	



<section class="middle-content great_contractors">
	<div class="why_hg">
		<div class="container">
			<div class="row pb-80">
				<div class="col-md-6 left_bdr ">
					<h2><?php the_field('hire_heading') ?></h2>
					<div class="left">
						<div class="text_left">
						<?php the_field('hire_content') ?>
						<!-- <p>Across the U.S. and in every industry, technology and engineering staffing and recruiting is changing more rapidly than ever.  Hiring and retaining the right talent is more difficult than ever before – not to mention staying on top of your other day-to-day responsibilities.</p>
						<p>Do your current staffing partners provide you the support and service you need to be successful?</p> -->
						</div>
						<a href="<?php the_field('learn_link') ?>" class="caps btn"><?php the_field('hire_link_text') ?></a>
					</div>
				</div>
				<div class="col-md-6 abs_right">
					<div class="right">
						<span>
							<img src="<?php the_field('hire_image') ?>" alt="">						
						</span>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="why_hg">
		<div class="container">
			<div class="row ">
				<div class="col-md-6 left_bdr pull-right ">
					<h2><?php the_field('find_job_heading') ?></h2>
					<div class="left">
						<div class="text_left">
						<?php the_field('find_job_content') ?>
						</div>
						<a href="<?php the_field('learn_more_link') ?>" class="caps btn"><?php the_field('learn_more_text') ?></a>
					</div>
				</div>
				<div class="col-md-6 abs_left">
					<div class="right">
						<span>
							<img src="<?php the_field('find_job_image') ?>" alt="">						
						</span>
					</div>
				</div>
			</div>
		</div>
		</div>
		
</section>

<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead' , 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();
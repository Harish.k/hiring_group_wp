<?php
/**
 * The front page template file
 * Template Name: Contractor-Benefits
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<!-- banner section starts here -->
<section>	
		<div class="benefits-banner">
			<div class="banner-text text-center">
				<h1><?php the_field('banner_title') ?></h1>
    			<h3><?php the_field('banner_sub_title') ?></h3>
			</div>
		</div>		
</section>

<section class="leadership con_benefits con_benefits_2 middle-content pt-80 pb-80 text-center" style="margin-top: 0;">
	<div class="container">
		<h2><?php the_field('page_heading') ?></h2>
		
		<div class="row">
			<?php

				// check if the repeater field has rows of data
				if( have_rows('benefits_circles') ):

				 	// loop through the rows of data
				    while ( have_rows('benefits_circles') ) : the_row();
				?>
					<div class="col-sm-4 text-center">
						<div class="facilities_div text-center">
							<h5><?php the_sub_field('benefit_circle_heading') ?></h5>
							<figure>
								<img src="<?php the_sub_field('benefit_circle_image') ?>" alt="Benefits">
							</figure>
						</div>
					</div>

				<?php

					endwhile;

					else :

					    // no rows found

					endif;

					?>

			<div class="col-sm-12 left text-center">
				<a href="<?php the_field('calculate_link') ?>" class="btn caps" target="_blank" ><?php the_field('calculate_link_text') ?></a>
			</div>
		</div>
	</div>
</section>

<section class="middle-content benefits-sec pb-80">
	<div class="container">
		<div class="row pb-80 pt-45">
			<h2><?php the_field('medical_benefits_heading') ?></h2>
			<div class="col-md-6">
				<ul>
					<?php

					// check if the repeater field has rows of data
					if( have_rows('medical_benefits_list')):

					 	// loop through the rows of data
					    while ( have_rows('medical_benefits_list') ) : the_row();
					?>
					<li class="<?php the_sub_field('list-link') ?>"><a href="<?php the_sub_field('list_main_link') ?>" target="_blank" ><?php the_sub_field('list_item') ?></a></li>
					
					<?php

					endwhile;

					else :

					    // no rows found

					endif;

					?>	
				</ul>
			</div>
			<div class="col-md-6">
				<div class="right">
					<a href="<?php the_field('medical_video') ?>" class="popup-youtube">
						<img src="<?php the_field('medical_video_image') ?>">
					</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<h2><?php the_field('dental_benefits_heading') ?></h2>
				<ul>
				<?php

					// check if the repeater field has rows of data
					if( have_rows('dental_benefits_list') ):

					 	// loop through the rows of data
					    while ( have_rows('dental_benefits_list') ) : the_row();
					?>
					<li><?php the_sub_field('list_item') ?></li>
					<?php

					endwhile;

					else :

					    // no rows found

					endif;

					?>	
				</ul>	
			</div>
			<div class="col-md-6">
				<div class="right">
 					<h2><?php the_field('vision_benefits_heading') ?></h2>
	 				<ul>
		 				<?php

						// check if the repeater field has rows of data
						if( have_rows('vision_benefits_list') ):

						 	// loop through the rows of data
						    while ( have_rows('vision_benefits_list') ) : the_row();
						?>
						<li><?php the_sub_field('list_item') ?></li>
						<?php

						endwhile;

						else :

						    // no rows found

						endif;

						?>	
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="middle-content see-yourself">
	<div class="container text-center">
		<div class="row pt-80 pb-80">
			<h2><?php the_field('see_yourself_heading') ?></h2>
			<a href="<?php the_field('calculate_link') ?>" class="btn caps" target="_blank" ><?php the_field('calculate_benefits_text') ?></a>
			<!-- <a href="https://secure.zenefits.com/benefitsPreview/requiredInformation?token=d5ed79a7-375d-465e-b4df-e9daeee6ff8a">Click here to calculate your benefits</a> -->
			<ul>

				<?php

					// check if the repeater field has rows of data
					if( have_rows('see_yourself_list') ):

					 	// loop through the rows of data
					    while ( have_rows('see_yourself_list') ) : the_row();
					?>
					<li><?php the_sub_field('list_item') ?></li>
					<?php

					endwhile;

					else :

					    // no rows found

					endif;

					?>	
				
			</ul>
		</div>
	</div>
</section>

<section class="middle-content benefits-sec add-on-benefits pt-80">
	<div class="container">
		<div class="row pb-80">
			<div class="col-md-6">
				<div class="left">
					<a href="<?php the_field('additional_benefits_video') ?>" class="vdo_img popup-youtube video_icon">
					<img src="<?php the_field('additional_benefits_image') ?>">
				</a>
				</div>
			</div>
			<div class="col-md-6">
				<h2><?php the_field('additional_benefits_heading') ?></h2>
				<ul>
					<?php

						// check if the repeater field has rows of data
						if( have_rows('additional_benefit_list') ):

						 	// loop through the rows of data
						    while ( have_rows('additional_benefit_list') ) : the_row();
						?>
						<li><?php the_sub_field('list_item') ?></li>
						<?php

						endwhile;

						else :

						    // no rows found

						endif;

						?>	
				</ul>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead' , 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();
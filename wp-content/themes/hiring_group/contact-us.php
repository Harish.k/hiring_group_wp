<?php
/**
 * The front page template file
 * Template Name: Contact_us
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<section>
	<!-- banner section starts here -->
		<div class="contact-banner">
			<h1><?php the_field('banner_title') ?></h1>
    		<h3><?php the_field('banner_sub_title') ?></h3>
		</div>		
</section>

<section>
	<div class="middle-content contact-mid pb-80">
		<div class="container">
			<h2><?php the_field('engage_form_heading') ?></h2>
			<P><?php the_field('engage_form_subhead') ?></P>
	        <div class="row form-row contact_form_2">
	             <!-- <form id="">
	             	<div class="col-md-6">
	             		<input type="text" name="" value="" placeholder="Full Name">
						<input type="text" name="" value="" placeholder="Phone">
						<input type="text" name="" value="" placeholder="Email">
	             	</div>
	             	<div class="col-md-6">
	             		<textarea name="" cols="" rows="" placeholder="Message"></textarea>
	             		<div>
	             			<input type="file" name="file" id="file" class="inputfile" />
							<label for="file">Attach a file(Resume, Job description etc.)</label>
						</div>
	             	</div>
	             	<div class="send-message-div">
	             		<button type="submit" name="">SEND</button>
	             	</div>	
	             </form> -->    
				<?php echo do_shortcode('[contact-form-7 id="371" title="Contact form 2"]') ?>
	        </div>

	        <div class="connect-social-media">
	        	<h2>CONNECT WITH US ON <span>SOCIAL MEDIA</span></h2>
	        	<ul class="list-unstyled list-inline">
	        		<li>
	        			<div class="social-circle fb-circle">
	        				<a href="<?php the_field('facebook_link') ?>" target="_blank"><span class="fa fa-facebook"></span></a>
	        			</div>
	        		</li>
	        		<li>	
	        			<div class="social-circle twitter-circle">
	        				<a href="<?php the_field('twitter_link') ?>" target="_blank"><span class="fa fa-twitter"></span></a>
	        			</div>
	        		</li>	
	        		<li>
	        			<div class="social-circle insta-circle">
	        				<a href="<?php the_field('insta_gram') ?>" target="_blank"><span class="fa fa-instagram"></span></a>
	        			</div>
	        		</li>
	        		<li>	
	        			<div class="social-circle linked-circle">
	        				<a href="<?php the_field('linked_in') ?>" target="_blank"><span class="fa fa-linkedin"></span></a>
	        			</div>
	        		</li>
	        	</ul>
	        </div>
   		</div>
	</div>
</section>
	
<section class="address-sec">
	<div class="container">
		<div class="row no-padding m-0">

			<?php

				// check if the repeater field has rows of data
				if( have_rows('contact_boxes') ):

				 	// loop through the rows of data
				    while ( have_rows('contact_boxes') ) : the_row();
			?>

			<div class="add-box">
				<div class="add-box-icon">
					<?php the_sub_field('address_icon') ?>
				</div>
				<div class="add-box-content">
					<h5><?php the_sub_field('address_heading') ?></h5>
					<p><?php the_sub_field('address_content') ?></p>
				</div>
			</div>

			<?php

				endwhile;

				else :

				    // no rows found

				endif;

			?>

		</div>
	</div>
</section>
	<div class="container pb-80">
		<div class="row map-container pt-45 pb-45">
			<div class="col-md-6">

				<?php the_field('map_1_link') ?>
			
			</div>

			<div class="col-md-6">

				<?php the_field('map_2_link') ?>

			</div>
		</div>
	</div>


<?php get_footer();

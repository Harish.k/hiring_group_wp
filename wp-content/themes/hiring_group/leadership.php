<?php
/**
 * The front page template file
 * Template Name: Leadership
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<!-- banner section starts here -->
<section>	
		<div class="leadership-banner">
			<div class="banner-text text-center">
				<h1><?php the_field('banner_title') ?></h1>
    			<h3><?php the_field('banner_sub_title') ?></h3>
			</div>
		</div>		
</section>

<section class="story-sec">
	<div class="middle-content">
		<h2><?php the_field('page_heading') ?></h2>
	</div>
	<section class="chris-sec">
		<div class="container no-padding">
			<div class="row pt-45 pb-45 m-0 leader-1-padding">
				<div class="col-md-4 no-padding">
					<div class="founder-box text-center">
						<img src="<?php the_field('founder_image') ?>" alt="">
						<h3><?php the_field('founder_name') ?></h3>
						<h5><?php the_field('founder_position') ?></h5>
						<div class="founder-social">
							<ul class="list-unstyled">
								<li><a href="mailto:<?php the_field('mail_link') ?>"><span class="fa fa-envelope"></span></a></li>
								<li><a href="<?php the_field('linked_in_link') ?>" target="_blank"><span class="fa fa-linkedin"></span></a></li>
							</ul>	
						</div>
					</div>
				</div>
				<div class="col-md-8 no-padding">
					<div class="founder-content">
						<?php the_field('about_founder') ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="chris-brooks-sec">
		<div class="container no-padding">
			<div class="row slider-inner-sec pt-80 m-0">
				<ul class="list-unstyled list-inline bxslider">
				<?php

					// check if the repeater field has rows of data
					if( have_rows('owner_slider') ):

					 	// loop through the rows of data
					    while ( have_rows('owner_slider') ) : the_row();
					?>
					<li>
						<div class="slider-main">
							<div class="slider-main-img">
								<img class="center-block" src="<?php the_sub_field('slider_image') ?>" alt="">
								<img class="center-block leader-logo" src="<?php the_sub_field('slider_logo') ?>" alt="">
							</div>
							<div class="slider-main-content">
								<?php the_sub_field('slider_content') ?>
					    	</div>	
					    </div>	
					 </li>


					<?php

					endwhile;

					else :

					    // no rows found

					endif;

					?>
					
				</ul>
			</div>
		</div>
	</section>
	<section class="brooks-sec">
		<div class="container no-padding">
			<div class="row pt-45 pb-45 m-0 leader-2-padding">
				<div class="col-md-4 no-padding visible-sm visible-xs">
					<div class="founder-box text-center">
						<img src="<?php the_field('founder_image_2') ?>" alt="">
						<h3><?php the_field('founder_name_2') ?></h3>
						<h5><?php the_field('founder_position_2') ?></h5>
						<div class="founder-social israel-social">
							<ul class="list-unstyled">
								<li><a href="mailto:<?php the_field('mail_link_2') ?>"><span class="fa fa-envelope"></span></a></li>
								<li><a href="<?php the_field('linked_in_2') ?>" target="_blank"><span class="fa fa-linkedin"></span></a></li>
							</ul>	
						</div>
					</div>
				</div>
				<div class="col-md-8 no-padding">
					<div class="founder-content">
						<?php the_field('about_founder_2') ?>
					</div>
				</div>
				<div class="col-md-4 no-padding hidden-sm hidden-xs">
					<div class="founder-box text-center">
						<img src="<?php the_field('founder_image_2') ?>" alt="">
						<h3><?php the_field('founder_name_2') ?></h3>
						<h5><?php the_field('founder_position_2') ?></h5>
						<div class="founder-social israel-social">
							<ul class="list-unstyled">
								<li><a href="mailto:<?php the_field('mail_link_2') ?>"><span class="fa fa-envelope"></span></a></li>
								<li><a href="<?php the_field('linked_in_2') ?>" target="_blank"><span class="fa fa-linkedin"></span></a></li>
							</ul>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>

<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead' , 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer();
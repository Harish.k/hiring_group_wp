<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Hiring_Group
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="error-message">
	<p class="number">404 !</p>
	<p>Page not found...</p>
</div>

<?php get_footer();

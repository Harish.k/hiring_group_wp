<?php
/*
 * Template Name: Search Jobs
*/
get_header(); ?>

<section>
	<!-- banner section starts here -->
	<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
	<div class="why-banner search-jobs" style="background-image: url('<?php echo $backgroundImg[0]; ?>');">
		<div class="banner-text text-center">
			<h1><?php the_field('banner_title') ?></h1>
			<h3><?php the_field('banner_sub_title') ?></h3>
		</div>
	</div>		
</section>

<section class="focus-sec" id="search_job">
	<div class="middle-content">
		<div class="container">
			<h2><?php the_field('banner_title') ?></h2>
		</div>	
	</div>
</section>

<section>
	<div class="container no-padding job_widget_top">
		<!-- <a class="post_job" href="#"><span><img src="assets/images/post_arrow.png" alt=""></span> POST A JOB</a> -->
		<div id="jobs-widget" style="width: 100%; height: 100%;">
			<div id="jobs_widget_container" style="overflow: hidden;">
				<h3> Current Job Opportunities (10):</h3>


				<div id="jobsTable_wrapper" class="dataTables_wrapper no-footer">
					<table id="jobsTable" style="margin-bottom: 20px; width: 100%;" class="dataTable no-footer" role="grid" width="100%">
						<colgroup>
						<col width="42%"> 
						<col width="20%">
						<col width="20%"> 
						<col width="18%">
					</colgroup>

					<thead>
						<tr role="row">
							<th class="sorting" tabindex="0" aria-controls="jobsTable" rowspan="1" colspan="1" style="width: 478.8px;" aria-label="Job Title: activate to sort column ascending">
								<?php the_field('job_title_heading'); ?>
							</th>
							<th class="sorting" tabindex="0" aria-controls="jobsTable" rowspan="1" colspan="1" style="width: 228px;" aria-label="Location: activate to sort column ascending">
								<?php the_field('location_heading'); ?>
							</th>
							<th class="sorting" tabindex="0" aria-controls="jobsTable" rowspan="1" colspan="1" style="width: 228px;" aria-label="Category: activate to sort column ascending">
								<?php the_field('category_heading'); ?>
							</th>
							<th class="sorting" tabindex="0" aria-controls="jobsTable" rowspan="1" colspan="1" style="width: 205.2px;" aria-label="Posted: activate to sort column ascending">
								<?php the_field('posted_heading'); ?>
							</th>
						</tr>
					</thead>

					<tbody>

						<?php 

						if( have_rows('current_job_company') ):

							while( have_rows('current_job_company') ) : the_row();?>
						<tr role="row" class="<?php the_sub_field('class_name'); ?>">
							<td>
								<a href="<?php the_sub_field('job_title_link'); ?>" target="_blank">
									<?php the_sub_field('job_title'); ?>
								</a>
							</td>
							<td data-headline="Location"><?php the_sub_field('location'); ?></td>
							<td data-headline="Category"><?php the_sub_field('category'); ?></td>
							<td class="date" data-headline="Posted"><?php the_sub_field('posted'); ?></td>
						</tr>

					<?php	endwhile;

					endif;

					?>


				</tbody>
			</table>
		</div> 
	</div>
</div>
</div>
</section>



<section>
	<div class="container-fluid no-padding">
		<div class="engage-sec pt-80 pb-80">
			<h3><?php the_field('form_heading', 4) ?></h3>
			<p><?php the_field('form_subhead' , 4) ?></p>
			<div class="container">
				<div class="row m-0">
					<div class="contact_form">
						<?php echo do_shortcode('[contact-form-7 id="97" title="Contact form 1"]') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer();

